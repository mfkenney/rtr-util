#!/usr/bin/env python
"""
engadc - read the RTR engineering data ADC
"""
import sys
from decimal import Decimal, getcontext


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b:a*x + b, self.C, 0)
        return y, self.units

# Use Decimal rather than float to avoid excessive precision
CALS = [
    (0, Calibration([Decimal('0.'), Decimal('6.56')], 'volts')),
    (1, Calibration([Decimal('0.'), Decimal('1.515')], 'amps')),
    (2, Calibration([Decimal('191.8'), Decimal('-185.8')], 'degC'))
]

# Volts per count
VPC = Decimal(5) / Decimal(4096)

# Voltage resolution
VRES = Decimal('0.001')

# /proc interface to A/D data
PROC_FILE = '/proc/iguana_adc'


def main():
    infile = PROC_FILE
    if len(sys.argv) > 1:
        infile = sys.argv[1]

    try:
        line = open(infile, 'r').read()
    except IOError:
        sys.stderr.write('Cannot access A/D\n')
        return 1

    f = line.strip().split()
    output = [f[0]]
    getcontext().prec = 4
    for chan, cal in CALS:
        x = (Decimal(f[chan+1]) * VPC).quantize(VRES)
        y, units = cal(x)
        output.append('{0} {1}'.format(str(y), units))
    print ','.join(output)
    return 0

if __name__ == '__main__':
    sys.exit(main())
