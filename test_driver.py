#!/usr/bin/env python
#
# Test program to monitor the GPS and on every fix, output the
# current location along with the A/D data.
#
import gps
import select
import time
from calendar import timegm
from decimal import Decimal, getcontext


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b:a*x + b, self.C, 0)
        return y, self.units

# Use Decimal rather than float to avoid excessive precision
CALS = [
    (0, Calibration([Decimal('0.'), Decimal('6.56')], 'volts')),
    (1, Calibration([Decimal('0.'), Decimal('1.515')], 'amps')),
    (2, Calibration([Decimal('191.8'), Decimal('-185.8')], 'degC'))
]

# Volts per count
VPC = Decimal(5) / Decimal(4096)

# Voltage resolution
VRES = Decimal('0.001')

# /proc interface to A/D data
PROC_FILE = '/proc/iguana_adc'


def read_adc(fname):
    line = open(fname, 'r').read()
    f = line.strip().split()
    output = [f[0]]
    getcontext().prec = 4
    for chan, cal in CALS:
        x = (Decimal(f[chan+1]) * VPC).quantize(VRES)
        y, units = cal(x)
        output.append('{0} {1}'.format(str(y), units))
    return ','.join(output)


def gps_reader(client):
    while True:
        rlist, _, _ = select.select([client.sock], [], [])
        if client.sock in rlist:
            if client.read() == -1:
                break
            if client.data['class'] == 'TPV':
                yield client.data


def main():
    mode = gps.WATCH_ENABLE | gps.WATCH_JSON | gps.WATCH_SCALED
    client = gps.gps(host='localhost',
                     port=2947,
                     mode=mode)
    fmt = '%Y-%m-%dT%H:%M:%S %Z'
    for datarec in gps_reader(client):
        t = timegm(time.strptime(datarec.time[:-5] + ' UTC', fmt))
        adc_data = read_adc(PROC_FILE)
        print 'P:{0:d} {1:.6f} {2:.6f}'.format(t, datarec.lat, datarec.lon)
        print 'E:{0}'.format(adc_data)


if __name__ == '__main__':
    main()
